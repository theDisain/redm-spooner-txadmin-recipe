# RedM Spooner txadmin recipe

Simple recipe to enable fast setup of Spooner. remember to use your own key and report to Leafy for any issues.

- Get latest FXServer binaries (https://runtime.fivem.net/artifacts/fivem/build_server_windows/master/)
- Run from executable (.exe for windows, you know what this means on linux)
- In recipe selection, either select custom template and copy in the recipe from the .yaml (paste in the whole .yml file) or choose custom link (https://gitlab.com/theDisain/redm-spooner-txadmin-recipe/-/raw/main/spooner-recipe.yml)
- When the recipe has finished and you've gone past the stage of server.cfg edit, the server won't start properly the first time. This is because spooner needs you to set gamename manually.
    - To do this you need to go to resources/spooner/fxmanifest.lua
    - In that file, you need to set the gamename to "rdr3", the fxmanifest file has directions on where to put it.
- Once you have fixed the fxmanifest file, restart your server and enjoy playing around in your mapping kingdom.